import React, { Component } from "react";
import { Tab, Segment } from "semantic-ui-react";
import UploadInput from "./UploadInput";
import UploadedFiles from "./UploadedFiles";
import { connect } from "react-redux";
import * as allActions from "../../store/actions/actionCreators";
import "../../css/upload.css";

class SignUp extends Component {
  panes = [
    {
      menuItem: " Upload Files",
      render: () => (
        <Tab.Pane>
          <UploadInput
            fileUpload={this.props.fileUpload}
            response={this.props.response}
          />
        </Tab.Pane>
      )
    },
    {
      menuItem: "Previousely Uploaded Files",
      render: () => (
        <Tab.Pane>
          <UploadedFiles
            fetchUpload={this.props.fetchUploadedFiles}
            files={this.props.files}
            response={this.props.response}
            deleteFile={this.props.deleteFile}
          />
        </Tab.Pane>
      )
    }
  ];

  render() {
    return (
      <Segment style={{ margin: "5% auto 0 auto", width: "90%" }}>
        <Tab panes={this.panes} />
      </Segment>
    );
  }
}
const mapState = state => {
  return { files: state.MainReducer.files, response: state.MainReducer.response };
};

export default connect(
  mapState,
  allActions
)(SignUp);
