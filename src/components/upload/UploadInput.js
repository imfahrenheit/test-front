import React, { Component, Fragment } from "react";
import { Image, Card, Segment, Button, Label, Input } from "semantic-ui-react";

const sizeCalc = (bytes, decimalPoint) => {
  if (bytes === 0) return "0 Bytes";
  let k = 1000,
    dm = decimalPoint || 2,
    sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
    i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
};

class UploadInput extends Component {
  state = {
    file: null,
    previewSrc: null,
    err: null,
    uploading: false,
    showAlert: false,
    fileSizeAlert: false
  };
  inputChangeHandler = e => {
    const file = e.target.files[0];
    if (file&&file.size < 4194304) {
      this.setState({ previewSrc: URL.createObjectURL(file), file });
    } else {
      this.setState({ fileSizeAlert: true });
      setTimeout(() => {
        this.setState({ fileSizeAlert:false });
      }, 2500);
      return false;
    }
  };
  // upload to server
  fileUploadHandler = () => {
    const {file}=this.state
    if (!file) {
     
      setTimeout(() => {
        this.setState({ validationError: false });
      }, 1500);
      return false;
    } else {
      const formData = new FormData();
      formData.set("file", this.state.file);
     
      this.props.fileUpload(formData)
      this.fileInput.value = null;
      this.setState({ file: null });
    }
   
  };
  //clears the file input
  cleaInput = () => {
    this.fileInput.value = null;
    this.setState({ file: null });
  };
  render() {
    const { file, fileSizeAlert  } = this.state;
    return (
      <Fragment>
        <div>
          <input
            style={{ display: "none" }}
            onChange={this.inputChangeHandler}
            ref={fileInput => (this.fileInput = fileInput)}
            type="file"
          />
         
          <Segment textAlign="center">
            <Label>
              <Input
                value={
                  !file
                    ? "Browse"
                    : ` Upload- ${
                        file.name.length < 18
                          ? this.state.file.name
                          : "Your File"
                      }`
                }
                disabled
                size="large"
                onFocus={() => {
                  this.fileInput.click();
                }}
              />

              {!this.state.file ? (
                <Button
                  icon="folder"
                  onClick={() => {
                    this.fileInput.click();
                  }}
                />
              ) : (
                <Fragment>
                  <Button
                    icon="upload"
                    color="teal"
                    onClick={this.fileUploadHandler}
                  />
                  <Button negative onClick={this.cleaInput} icon="trash" />
                </Fragment>
              )}
            </Label>
            <br/>
            {fileSizeAlert && <Label color='orange' pointing > Maximum file size is 5mb </Label>}
             

            {file && (
              <Card centered>
                <Image
                  src={this.state.previewSrc}
                  alt=""
                  style={{ maxWidth: "100%" }}
                />
                <Card.Content>
                  <Card.Header>{file.name}</Card.Header>
                  <Card.Meta>{sizeCalc(file.size)}</Card.Meta>
                  <Card.Description>
                    You are going to upload {file.name}
                  </Card.Description>
                </Card.Content>
              </Card>
            )}
          </Segment>
        </div>
      </Fragment>
    );
  }
}
export default UploadInput;
