import React, { Component } from "react";
import { Icon} from "semantic-ui-react";
import axios from "axios";

const url = "http://localhost:5000/";
const createHeaders = token => {
  return { headers: { Authorization: token ? `Bearer ${token}` : "" }, responseType: "blob" };
};

export default class UploadInput extends Component {
  state = {
    file: null,
    previewSrc: null,
    uploading: false,
    ileSizeAlert: false
  };
  componentDidMount() {
    this.props.fetchUpload();
  }

  download = el => {
   const FileSaver = require("file-saver");
    const token = localStorage.getItem("token");
    const headers = createHeaders(token);
    axios
      .get(`${url}${el.file}`, headers)
      .then(res => {
        const file = new Blob([res.data], {
          type: res.type});
       FileSaver.saveAs(file,el.name);
      })
      .catch(err => console.log(err));
  };

  displayImg = () => {
    const { files } = this.props;
    if (files) {
      return files.map(el => {
        return <div key={el._id} className="item_card">
            <h3>{el.name}</h3>
            <small>{el.size}</small>
            <hr />
            <div>
              <span onClick={() => this.props.deleteFile(el._id)} className="action-icon" >
                <Icon name="trash" />
              </span>
              <span onClick={() => this.download(el)} className="action-icon">
                <Icon name="arrow alternate circle down outline" />
              </span>
            </div>
          </div>;
      });
    } else return <span />;
  };

  render() {
    const { response } = this.props;
    return <div style={{ textAlign: "center" }}>
        <div className="img-container">
          {this.displayImg()}
        </div>

        {response && <h3 style={{ margin: "0 auto" }}>
                {response}
          </h3>}
      </div>;
  }
}
