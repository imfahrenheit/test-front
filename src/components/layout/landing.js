import React from 'react'
import { Segment,Header,Button } from "semantic-ui-react";
import {Link } from 'react-router-dom'
const landing = () => {
  return <div>
      <Segment style={{ textAlign: "center", marginTop: "50px" }}>
        <Header>Please LogIn or SignUp For uploading your files </Header> 
          <div style={{ textAlign: "center", margin: "10px auto 0 auto" , width:'50%'}}>
          <Button as={Link} to="/login" color='grey' content="Log In" fluid /> <br />
              <Button as={Link} to="/signup" color='grey' content="Sing up" fluid />
        </div>
      </Segment>
    </div>;
}

export default landing
