import React, { Component,Fragment } from 'react'
import * as allActions  from '../../store/actions/actionCreators'
import Registration from '../upload/Registration'
import Landing from './landing.js'
import {connect } from 'react-redux'



 class Home extends Component {

  render() {
      const {isLoggedIn} = this.props
    return <Fragment>
       {!isLoggedIn ? <Landing/> :<Registration/>}
        



      </Fragment>
  }
}
const mapState = state => {
  return { isSignedUp: state.MainReducer.isSignedUp, isLoggedIn: state.MainReducer.isLoggedIn };
};

export default connect(mapState,allActions)(Home)
