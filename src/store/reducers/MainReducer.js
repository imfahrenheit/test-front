import * as actionTypes from "../actions/actionTypes";

const initialState = {
  isLoggedIn:false,
  isSignedUp:false,
  serverErrors:null,
  user:null,
  files:null,
  response:''

};

export const MainReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTH_USER:
      if (action.user) {
        return { ...state, isLoggedIn: true, user: action.user };
      }
      return { ...state };
    case actionTypes.LOGOUT_USER:
      return { ...state, isLoggedIn: false, user: null, files:null, response:"",serverErrors:null };
    case actionTypes.STORE_FETCHED_FILES:
      return { ...state, files: action.files };
    case actionTypes.USER_SIGN_UP:
      return { ...state, isSignedUp: true };
    case actionTypes.SAVE_ERRORS:
      return { ...state, serverErrors: action.err };
    case actionTypes.DELETE_FILE:
      return { ...state, files: state.files.filter(file => file._id !== action.id) };
    case actionTypes.SERVER_RESPONSE:

      return {...state, response: action.res };
    default:
      return state;
  }
};
