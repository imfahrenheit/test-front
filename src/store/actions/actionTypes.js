export const AUTH_USER = 'AUTH_USER';
export const INIT_STORE_USER = "INIT_STORE_USER";
export const USER_SIGN_UP = "USER_SIGN_UP";
export const LOGOUT_USER = "LOGOUT_USER";
export const INIT_USER_SIGN_UP = "INIT_USER_SIGN_UP";
export const SAVE_ERRORS = "SAVE_ERRORS";
export const UPLOAD_FILE = "UPLOAD_FILE";
export const FETCH_UPLOAD = "FETCH_UPLOAD";
export const STORE_FETCHED_FILES = "STORE_FETCHED_FILES ";
export const DELETE_FILE = "DELETE_FILE";
export const SERVER_RESPONSE = "SERVER_RESPONSE";



