import { takeLatest } from "redux-saga/effects";
import * as actionType from "../actions/actionTypes";
import * as allSagas from "./MainSaga";
export function* watcherSaga() {
  yield takeLatest(actionType.INIT_STORE_USER, allSagas.storeUserSaga);
  yield takeLatest(actionType.INIT_USER_SIGN_UP, allSagas.userSignUpSaga);
  yield takeLatest(actionType.UPLOAD_FILE, allSagas.fileUploadSaga);
  yield takeLatest(actionType.FETCH_UPLOAD, allSagas.fetchUploadSaga);
  yield takeLatest(actionType.DELETE_FILE, allSagas.deleteFileSaga);
}
