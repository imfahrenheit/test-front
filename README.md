# Frontend



The frontend UI is created with react and other helper libraries 

  - The idea was building a minimalistic UI with React and semantic-ui-react
  - There are still lot to improve in term of Ux
  


### Tech

The following techs are used in the frontend

* [React js]
* [Redux]-for state management
* [Redux-saga] - for async side effects handling via redux action
* [semantic-ui-react]- great UI lib for modern web apps




### Installation

Install the dependencies and devDependencies and start the server.

```sh
clone the repository

$ cd [directory]
$  npm install
$ yarn start
```












